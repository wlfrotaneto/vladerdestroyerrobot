# VladerDestroyer

Robô do [Robocode](https://robocode.sourceforge.io) desenvolvido por Walter Lopes Frota Neto, Engenharia de Software - UCSal, para o Solutis Robot Arena.

VladerDestroyer se baseia no mecanismo de TurnMultiplier para manter a mira sempre direcionada ao inimigo. Isso torna ele preciso, causando um alto dano. Para evitar ser atingido constantemente, ele se movimenta esporadicamente. Testado em diversas batalhas, ele se provou consistente com bons resultados.

Encontrei dificuldade no que tange a execução das funções nativas do AdvancedRobot, visto que após algumas movimentações, o robô interrompe todo seu funcionamento dentro do loop. Para contornar a situação, tive que forçar o movimento dentro do loop através de um boolean que se torna verdadeiro após esses execução desses momentos que impedem o funcionamento.

**Pontos Fortes**
- Mira precisa
- Durabilidade no combate

**Pontos Fracos**
- Movimentação instável e lenta

Gostei muito de conhecer essa plataforma, e de perceber o grande universo de possibilidades e personalidades que é possível dar ao seu robô, abordando diferentes estratégias em busca da mais efetiva, ou até, da mais divertida.

![VladerDestroyer](https://i.imgur.com/99hfyhm.png)