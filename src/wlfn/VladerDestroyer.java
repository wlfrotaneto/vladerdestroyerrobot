package wlfn;
import robocode.*;
import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import java.awt.*;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * VladerDestroyer - a robot by Walter Neto
 */
public class VladerDestroyer extends AdvancedRobot
{

	int shootErrors = 0;
	boolean turnError = false;
	
	/**
	 * run: VladerDestroyer's default behavior
	 */
	public void run() {
	
		// Cores
		setBodyColor(Color.black);
		setGunColor(Color.red);
		setRadarColor(Color.white);
		setScanColor(Color.white);
		setBulletColor(Color.red);
		
		// Robot main loop
		setTurnGunRightRadians(Double.POSITIVE_INFINITY);
		do {
			if(turnError == true) {
				setTurnGunRightRadians(Double.POSITIVE_INFINITY);
				turnError = false;
			}	
			scan();
		} while (true);	
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e){
    	
		double gunTurn = getHeadingRadians() + e.getBearingRadians() - getGunHeadingRadians();

		setTurnGunRightRadians(Utils.normalRelativeAngle(gunTurn));

		double distance = e.getDistance();
	
		if (distance > 600) {
			setTurnRight(Utils.normalRelativeAngle(gunTurn));
			setAhead(400);
			//turnError = true;
		}		

	    if (distance < 300) {
        	setFire(3);
    	}
   		else if (distance < 500) {
        	setFire(2);
    	} else {
        	setFire(1);
    	}
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e){
		double energy = getEnergy();
    	//double bearing = e.getBearing();   
	    //setTurnRight(-bearing);
		if(energy < 80){
			setAhead(150);
		} else {
	    	setAhead(50);
		}
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e){
    	double bearing = e.getBearing();
	    setTurnRight(-bearing);
	    setAhead(100);
		turnError = true;
		scan();
	}
	
	public void onBulletMissed(BulletMissedEvent e) {
		shootErrors++;
		if (shootErrors >= 2){
			turnError = true;
		} else if(shootErrors >= 3){
			setAhead(100);
			//setTurnRight(20);
			shootErrors = 0;
		}
	}
	
	public void onBulletHit(BulletHitEvent e){
		shootErrors = 0;
	}
	
	public void onWin(WinEvent e) {
		for (int i = 0; i < 10; i++) {
			turnRight(100);
		}
	}

}